FROM purban/centos7_java8

ADD src /opt/src

RUN yum install -y /opt/src/scala-2.11.7.rpm \
    && yum clean all \
    && rm -rf /opt/src/*

COPY src/HelloWorld.scala /opt/src/HelloWorld.scala

ENTRYPOINT [ "scala" ]

WORKDIR /opt/src

CMD [ "HelloWorld.scala" ]


